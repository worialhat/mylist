#ifndef LIST_H
#define LIST_H

#include <stddef.h>

template <typename _Tp> struct My_List_node;
template <typename _Tp> struct My_List_iterator;
template <typename _Tp> struct My_List_const_iterator;
template <typename _Tp> struct My_List_reverse_iterator;
template <typename _Tp> struct My_List_const_reverse_iterator;

template <typename _Tp>
struct My_List_node{
    My_List_node *next;
    My_List_node *prev;
    _Tp data;
    My_List_node() : next(NULL), prev(NULL) {}
};

template <typename _Tp>
struct My_List_iterator{
    typedef My_List_iterator<_Tp>                 _Self;
    typedef My_List_node<_Tp>                     _Node;
    typedef _Tp*                                pointer;
    typedef _Tp&                                reference;
    My_List_iterator() : node() {}
    My_List_iterator(_Node *n) : node(n) {}
    My_List_const_iterator<_Tp> base() { return My_List_const_iterator<_Tp>(node); }
    reference operator*() const { return (node->data); }
    pointer operator->() const { return &(node->data); }
    _Self & operator++() {
        node = node->next;
        return *this;
    }
    _Self operator++(int) {
        _Self __tmp = *this;
        node = node->next;
        return __tmp;
    }
    _Self & operator--() {
        node = node->prev;
        return *this;
    }
    _Self operator--(int) {
        _Self __tmp = *this;
        node = node->prev;
        return __tmp;
    }
    bool operator==(const _Self& __x) const { return node==__x.node; }
    bool operator!=(const _Self& __x) const { return node!=__x.node; }
    _Node * node;
};

template <typename _Tp>
struct My_List_const_iterator{
    typedef My_List_const_iterator<_Tp>          _Self;
    typedef const My_List_node<_Tp>              _Node;
    typedef const _Tp*                         pointer;
    typedef const _Tp&                         reference;
    My_List_const_iterator() : node() {}
    My_List_const_iterator(const _Node *n) : node(n) {}
    My_List_const_iterator<_Tp> base() { return My_List_const_iterator<_Tp>(node); }
    reference operator*() const { return node->data; }
    pointer operator->() const { return &(node->data); }
    _Self & operator++() {
        node = node->next;
        return *this;
    }
    _Self operator++(int) {
        _Self __tmp = *this;
        node = node->next;
        return __tmp;
    }
    _Self & operator--() {
        node = node->prev;
        return *this;
    }
    _Self operator--(int) {
        _Self __tmp = *this;
        node = node->prev;
        return __tmp;
    }
    bool operator==(const _Self& __x) const { return node==__x.node; }
    bool operator!=(const _Self& __x) const { return node!=__x.node; }
    const _Node * node;
};

template <typename _Tp>
struct My_List_reverse_iterator{
    typedef My_List_reverse_iterator<_Tp>         _Self;
    typedef My_List_node<_Tp>                     _Node;
    typedef _Tp*                                pointer;
    typedef _Tp&                                reference;
    My_List_reverse_iterator() : node() {}
    My_List_reverse_iterator(_Node *n) : node(n) {}
    My_List_const_iterator<_Tp> base() { return My_List_const_iterator<_Tp>(node); }
    reference operator*() const { return node->data; }
    pointer operator->() const { return &(node->data); }
    _Self & operator++() {
        node = node->prev;
        return *this;
    }
    _Self operator++(int) {
        _Self __tmp = *this;
        node = node->prev;
        return __tmp;
    }
    _Self & operator--() {
        node = node->next;
        return *this;
    }
    _Self operator--(int) {
        _Self __tmp = *this;
        node = node->next;
        return __tmp;
    }
    bool operator==(const _Self& __x) const { return node==__x.node; }
    bool operator!=(const _Self& __x) const { return node!=__x.node; }
    _Node * node;
};

template <typename _Tp>
struct My_List_const_reverse_iterator{
    typedef My_List_const_reverse_iterator<_Tp>  _Self;
    typedef const My_List_node<_Tp>              _Node;
    typedef const _Tp*                         pointer;
    typedef const _Tp&                         reference;
    My_List_const_reverse_iterator() : node() {}
    My_List_const_reverse_iterator(const _Node *n) : node(n) {}
    My_List_const_iterator<_Tp> base() { return My_List_const_iterator<_Tp>(node); }
    reference operator*() const { return node->data; }
    pointer operator->() const { return &(node->data); }
    _Self & operator++() {
        node = node->prev;
        return *this;
    }
    _Self operator++(int) {
        _Self __tmp = *this;
        node = node->prev;
        return __tmp;
    }
    _Self & operator--() {
        node = node->next;
        return *this;
    }
    _Self operator--(int) {
        _Self __tmp = *this;
        node = node->next;
        return __tmp;
    }
    bool operator==(const _Self& __x) const { return node==__x.node; }
    bool operator!=(const _Self& __x) const { return node!=__x.node; }
    const _Node * node;
};


/// интерфейс двухсвязного списка
template <typename _Tp>
class List
{
public:

    typedef My_List_node<_Tp>                             _Node;
    typedef My_List_iterator<_Tp>                         iterator;
    typedef My_List_const_iterator<_Tp>                   const_iterator;
    typedef My_List_reverse_iterator<_Tp>                 reverse_iterator;
    typedef My_List_const_reverse_iterator<_Tp>           const_reverse_iterator;

    List() {}
    virtual ~List() {}

    virtual iterator begin() = 0;
    virtual iterator end() = 0;
    virtual const_iterator cbegin() const = 0;
    virtual const_iterator cend() const = 0;
    virtual reverse_iterator rbegin() = 0;
    virtual reverse_iterator rend() = 0;
    virtual const_reverse_iterator crbegin() const = 0;
    virtual const_reverse_iterator crend() const = 0;

    virtual void push_back(const _Tp &) = 0;                    /// добавление в конец списка
    virtual void push_front(const _Tp &) = 0;                   /// добавление в начало списка
    virtual void pop_back() = 0;                                /// удаление последнего элемента списка
    virtual void pop_front() = 0;                               /// удаление первого элемента списка
    virtual void insert(const_iterator, const _Tp &) = 0;   	/// метод вставки элемента
    virtual const _Tp & front() const = 0;					    /// возвращает ссылку на первый элемент списка
    virtual const _Tp & back() const = 0;      					/// возвращает ссылку на последний элемент списка
    virtual void clear() = 0;                                   /// очищает список
    virtual size_t size() const = 0;                            /// возвращает кол-во элементов в списке
    virtual size_t max_size() const = 0;                        /// макс. кол-во элементов в списке
    virtual bool empty() const = 0;                             /// проверка на пустоту списка
    virtual bool full() const = 0;                              /// проверка на полноту списка

};


/// @brief список построенный на массивах
/// у нас есть пул размером из которого беру узлы
/// вместо того чтобы динамически выделять под них память
/// пул задается извне методом set_heap
template <typename _Tp>
class StaticList : public List<_Tp>
{
public:
    typedef List<_Tp>                               _Base;
    typedef My_List_node<_Tp>                         _Node;
    typedef My_List_iterator<_Tp>                     iterator;
    typedef My_List_const_iterator<_Tp>               const_iterator;
    typedef My_List_reverse_iterator<_Tp>             reverse_iterator;
    typedef My_List_const_reverse_iterator<_Tp>       const_reverse_iterator;

private:

    _Node * heap;                               /// пул из которого беру берутся новые узлы
    size_t maxSize;                             /// макс. кол-во элементов в списке
    size_t heapIndex;                           /// индекс последнего элемента пула
    size_t nodeCount;                           /// кол-во элементов в списке

    My_List_node<_Tp> *_back, *_front;
    _Tp null_elem;  /// элемент, ссылка на который возвращается в методах back() и front() когда список пуст

    int getNode(_Node ** _node){
        if(heapIndex<maxSize){
            *_node = &heap[heapIndex++];
            return 1;
        }
        else{
            /// если пул не полный, но последний индекс ссылается на конец пула - значит есть пропуски, поэтому ищу их
            if( !full() ){
                for( size_t i=0; i<maxSize; ++i ){
                    if( heap[i].next==NULL && heap[i].prev==NULL ) {    // ищу свободный узел
                        if ( &heap[i]!=_front ){                 // и проверяю не голова ли он(хз)
                            *_node = &heap[heapIndex++];
                            return 1;
                        }
                    }
                }
            }
        }
        return 0;
    }

public:

    StaticList(_Node * heap = 0, const size_t maxSize = 0) :
        heap(heap),
        maxSize(maxSize)
    {
        clear();
    }

    ~StaticList() {
    }

    iterator begin() { return iterator(_front); }
    iterator end() { return _back?iterator(_back->next):NULL; }
    const_iterator cbegin() const { return const_iterator(_front); }
    const_iterator cend() const { return _back?const_iterator(_back->next):NULL; }
    reverse_iterator rbegin() { return reverse_iterator(_back); }
    reverse_iterator rend() { return _front?reverse_iterator(_front->prev):NULL; }
    const_reverse_iterator crbegin() const { return const_reverse_iterator(_back); }
    const_reverse_iterator crend() const { return _front?const_reverse_iterator(_front->prev):NULL; }

    const _Tp & front() const { return empty()?null_elem:_front->data; }    /// возвращает ссылку на первый элемент списка
    const _Tp & back() const { return empty()?null_elem:_back->data; }      /// возвращает ссылку на последний элемент списка

    void set_heap(_Node *new_heap, size_t size){
        heap = new_heap;
        maxSize = size;
        clear();
    }

    void push_back(const _Tp & __x){
        if( !full() ){
            _Node * newNode;
            if( getNode(&newNode) ){    /// беру узел из пула
                newNode->data = __x;
                newNode->next = NULL;
                if( empty() ){
                    _front = newNode;
                    newNode->prev = NULL;
                }
                else{
                    _back->next = newNode;
                    newNode->prev = _back;
                }
                _back = newNode;
                nodeCount++;
            }
        }
    }

    void push_front(const _Tp & __x){
        if( !full() ){
            _Node * newNode;
            if( getNode(&newNode) ){    /// беру узел из пула
                newNode->data = __x;
                newNode->prev = NULL;
                if( empty() ){
                    _back = newNode;
                    newNode->next = NULL;
                }
                else{
                    _front->prev = newNode;
                    newNode->next = _front;
                }
                _front = newNode;
                nodeCount++;
            }
        }
    }

    void pop_back(){
        if( !empty() ){
            _Node * prev = _back->prev;
            if( prev==NULL ){
                clear();
            }
            else{
                _back->next = _back->prev = NULL;
                _back = prev;
                nodeCount--;
            }
        }
    }

    void pop_front(){
        if( !empty() ){
            _Node * next = _front->next;
            if( next==NULL ){
                clear();
            }
            else{
                _front->next = _front->prev = NULL;
                _front = next;
                nodeCount--;
            }
        }
    }

    void insert(const_iterator it, const _Tp & __x){
		if( !full() ){
			if( it == this->cbegin() ){
				this->push_front(__x);
			}
			else{
				_Node * newNode;
				if( getNode(&newNode) && it.node ){    /// беру узел из пула
					newNode->data = __x;
					newNode->prev = it.node->prev;
					newNode->next = it.node->prev->next;
					/// дичайшая дичь, не знаю как получить доступ к элементу на который ссылается итератор
					it.node->prev->next = newNode;
					it.node->prev->next->next->prev = newNode;
					nodeCount++;
				}
			}
		}
    }

    void clear(){
        nodeCount = heapIndex = 0;
        _back = _front = NULL;
        for(size_t i=0; i<max_size(); i++){
            heap[i].next = heap[i].prev = NULL;
        }
    }

    size_t size() const {
        return nodeCount;
    }

    size_t max_size() const {
        return maxSize;
    }

    bool empty() const {
        return nodeCount == 0;
    }

    bool full() const {
        return nodeCount == maxSize;
    }

};



#endif // LIST_H
