#include <iostream>
#include <list.h>

using namespace std;

int main(int argc, char *argv[])
{
    cout << "Hello World!" << endl;

    StaticList<int> mylist;
    mylist.push_back(42);
    mylist.push_back(24);
    mylist.push_back(13);

    int k = 0;

    StaticList<int>::iterator it=mylist.begin();

    (*it) = 11;
    cout << (*it) << " ";

//    for(StaticList<int>::iterator it=mylist.begin(); it!=mylist.end(); ++it)
    {
//        cout << k++ << " ";
    }

    return 0;
}
